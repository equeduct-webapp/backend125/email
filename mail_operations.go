package email

import (
	"fmt"
	"os"
	"strconv"
	"crypto/tls"
	"gopkg.in/gomail.v2"
)

func SendEmail(to string, subject string, body string) {

	// from is senders email address
	
	// we used environment variables to load the
	// email address and the password from the shell
	// you can also directly assign the email address
	// and the password
	host := os.Getenv("SMTP_HOST") // "smtp.gmail.com"
	from := os.Getenv("SMTP_FROM_EMAIL")
	password := os.Getenv("SMTP_PASSWORD")
	port := os.Getenv("SMTP_PORT") // 587 Default

	portInt,_ := strconv.Atoi(port)

	fmt.Println(host)
	fmt.Println(from)
	fmt.Println("password? NoNo")
	fmt.Println(portInt)


	m := gomail.NewMessage()
	m.SetHeader("From", from)
	m.SetHeader("To", to)
	//m.SetAddressHeader("Cc", "dan@example.com", "Dan")
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", body)
	//m.Attach("/home/Alex/lolcat.jpg")

	d := gomail.NewDialer(host, portInt, from, password)
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}


	// Send the email to Bob, Cora and Dan.
	if err := d.DialAndSend(m); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Mail sent successfully....")
	}
}
